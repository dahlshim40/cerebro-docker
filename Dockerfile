FROM openjdk:11-jre-slim

ENV CEREBRO_VERSION 0.9.4

COPY entrypoint.sh /usr/local/bin/entrypoint.sh 
RUN chmod +x /usr/local/bin/entrypoint.sh \
 &&  apt-get update \
 && apt-get install -y wget \
 && rm -rf /var/lib/apt/lists/* \
 && mkdir -p /opt/cerebro_orig/logs \
 && mkdir -p /opt/cerebro/certs \
 && wget -qO- https://github.com/lmenezes/cerebro/releases/download/v${CEREBRO_VERSION}/cerebro-${CEREBRO_VERSION}.tgz \
  | tar xzv --strip-components 1 -C /opt/cerebro_orig \
 && sed -i '/<appender-ref ref="FILE"\/>/d' /opt/cerebro_orig/conf/logback.xml \
 && addgroup -gid 1000 cerebro \
 && adduser -gid 1000 -uid 1000 cerebro \
 && chown -R cerebro:cerebro /opt/cerebro

WORKDIR /opt/cerebro
USER root

ENTRYPOINT [ "/usr/local/bin/entrypoint.sh" ]
