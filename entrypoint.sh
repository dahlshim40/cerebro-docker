#!/bin/sh

if [ -d "/opt/cerebro" ]
then
  cp -R /opt/cerebro_orig/* /opt/cerebro
fi

if [ ! -z "$(ls -A /opt/cerebro/certs)" ]
then
  cp -R /opt/cerebro/certs/* /usr/local/share/ca-certificates
  update-ca-certificates
fi

su cerebro -c '/opt/cerebro/bin/cerebro'
